const noteTemplate = document.getElementById('note-template');
const boardTemplate = document.getElementById('board-template');
const notesArea = document.querySelector('.notes-area');
const boardsArea = document.querySelector('.board-nav');
const addNoteBtn = document.querySelector('.add-note');
const addBoardBtn = document.querySelector('.add-board');
const deleteBoardBtn = document.querySelector('.delete-board');

// Those are data for first-use only.
let boardsList = [{
    id: 1,
    name: "board X",
    selected: false
}, {
    id: 2,
    name: "board YGHaid",
    selected: true
}]
let notesList = {
    0: [{
            id: 1,
            color: 'white',
            text: "first Note",
            xPosition: 500,
            yPosition: 500,
        }, {
            id: 2,
            color: 'white',
            text: "second Note",
            xPosition: 100,
            yPosition: 100,
        }, {
            id: 3,
            color: 'white',
            text: "third Note",
            xPosition: 200,
            yPosition: 200,
        }],
    1: [{
        id: 10,
        color: 'white',
        text: "first Note",
        xPosition: 500,
        yPosition: 500,
        }, {
            id: 12,
            color: 'white',
            text: "second Note",
            xPosition: 100,
            yPosition: 100,
        }, {
            id: 13,
            color: 'white',
            text: "third Note",
            xPosition: 200,
            yPosition: 200,
        }]
};

// Getting the items if they exist in local-storage and rendering HTML.
const LOCAL_STORAGE_LIST_KEY = 'notes.list';
const LOCAL_STORAGE_LIST2_KEY = 'boards.list';
notesList = JSON.parse(localStorage.getItem(LOCAL_STORAGE_LIST_KEY)) || notesList;
boardsList = JSON.parse(localStorage.getItem(LOCAL_STORAGE_LIST2_KEY)) || boardsList;
save();

// Adding a new note.
addNoteBtn.addEventListener('click', event => {
    let newNote = {
        id: notesList[getIndexOfCurrentBoard()].length == 0 ? 1 : notesList[getIndexOfCurrentBoard()][notesList[getIndexOfCurrentBoard()].length-1].id + 1,
        color: 'white',
        text: "New Note",
        xPosition: 50,
        yPosition: 60,
    };
    
    // Finding the selected board and adding the note to it.
    notesList[getIndexOfCurrentBoard()].push(newNote);
    save();
});

// Adding a new board
addBoardBtn.addEventListener('click', event => {

    // The added board will be selected automatically.
    let newBoard = {
        id: boardsList.length == 0 ? 0 : boardsList[boardsList.length-1].id + 1,
        name: "New Board",
        selected: true
    }
    
    // Setting all other boards to be unselected.
    boardsList.forEach(board => {
        board.selected = false;
    })
    
    // Adding the new board to boardsList and initializing it with one note.
    boardsList.push(newBoard);
    notesList[Object.keys(notesList).length] = [{
        id: 0,
        color: 'white',
        text: "New Note",
        xPosition: 50,
        yPosition: 60,
    }];

    save();
})

deleteBoardBtn.addEventListener('click', event => {
    let index = getIndexOfCurrentBoard();
    boardsList.splice(index, 1);
    save();
})

// Rendering HTML page using boards and notes lists.
function renderHTMLPage() {
    notesArea.innerHTML = "";
    renderBoards();
}

function renderBoards() {
    boardsArea.innerHTML ="";

    boardsList.forEach( (board, boardIndex) => {
        let elementData  = boardTemplate.content.querySelector('button');
        let currentMember = elementData.cloneNode(true);

        currentMember.innerHTML = board.name;
        // Checking for the selected board and rendering corresponding notes.
        if(board.selected){
            currentMember.classList.add('selected-tab');
            renderLists(notesList[boardIndex]);
        }

        // Adding functionality to the board when clicked. If it's not selected we want it to become selected.
        currentMember.addEventListener('click', event => {
            if(boardIndex == getIndexOfCurrentBoard())
                return;
            
            // Unselecting other boards.
            boardsList.forEach(board => {
                board.selected = false;
            })

            // Selecting the one clicked.
            boardsList[boardIndex].selected = true;
            save();
        })

        // Checking if the board name got changed.
        checkForEditions(currentMember, boardIndex, "name");
        boardsArea.appendChild(currentMember);
    })
}

function renderLists(selectedList) {
    selectedList.forEach( (note, index) => {

        // Getting the template data to create a note.
        let elementData  = noteTemplate.content.querySelector('.note');
        let currentMember = elementData.cloneNode(true);

        currentMember.querySelector('.note-text').innerText = note.text;
        // Here I should be adding the position to the corresponding css.
        currentMember.setAttribute("style", "top: "+ note.yPosition + "px; left: "+ note.xPosition + "px;");
        
        // Putting the color according to note color attribute.
        currentMember.classList.add(note.color);

        // Setting all color and close buttons of the current note to work. 
        addFunctionality(currentMember, index);

        // Checking if the note text got changed.
        checkForEditions(currentMember.querySelector('.note-text'), index, "text");
        dragElement(currentMember, index);
        notesArea.appendChild(currentMember);
    })
}

function addFunctionality(noteElement, index) {
    // dragAndDropFunctionality(noteElement);
    // Selecting all the buttons inside the note.
    whiteButton = noteElement.querySelector('.circle.white');
    redButton = noteElement.querySelector('.circle.red');
    greenButton = noteElement.querySelector('.circle.green');
    blueButton = noteElement.querySelector('.circle.blue');
    deleteNote = noteElement.querySelector('.circle.close');

    // Creating the effect of each button.
    whiteButton.addEventListener('click', event => {
        notesList[getIndexOfCurrentBoard()][index].color = 'white';
        save();
    });
    redButton.addEventListener('click', event => {
        notesList[getIndexOfCurrentBoard()][index].color = 'red';
        save();
    })
    greenButton.addEventListener('click', event => {
        notesList[getIndexOfCurrentBoard()][index].color = 'green';
        save();
    })
    blueButton.addEventListener('click', event => {
        notesList[getIndexOfCurrentBoard()][index].color = 'blue';
        save();
    })
    deleteNote.addEventListener('click', event => { 
        notesList[getIndexOfCurrentBoard()].splice(index, 1);
        save();
    })
}

// This function gets a certain html Element with the corresponding index inside the lists.
// And then checks if we edited the content using contenteditable='true' and then saves the effect.
function checkForEditions(currentMember, index, type) {
    currentMember.addEventListener('focusout', event => {
        var editedContent   = currentMember.innerText;
        if (type == "name")
            boardsList[index].name = editedContent;
        else if(type == "text")
            notesList[getIndexOfCurrentBoard()][index].text = editedContent;

        save();
    })
}

function save(){
    localStorage.setItem(LOCAL_STORAGE_LIST_KEY, JSON.stringify(notesList));
    localStorage.setItem(LOCAL_STORAGE_LIST2_KEY, JSON.stringify(boardsList));
    renderHTMLPage();
}

// This function gets the index of the selected board.
function getIndexOfCurrentBoard() {
    for(var i = 0; i < boardsList.length; i++ ) {
        if(boardsList[i].selected){
            return i;
        }
    }
}

function dragElement(note, index) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
    moveBar = note.querySelector('.move-bar');
    moveBar.onmousedown = dragMouseDown;
  
    function dragMouseDown(e) {
      e = e || window.event;
      e.preventDefault();
      // get the mouse cursor position at startup:
      pos3 = e.clientX;
      pos4 = e.clientY;
      document.onmouseup = closeDragElement;
      // call a function whenever the cursor moves:
      document.onmousemove = elementDrag;
    }
  
    function elementDrag(e) {
      e = e || window.event;
      e.preventDefault();
      // calculate the new cursor position:
      pos1 = pos3 - e.clientX;
      pos2 = pos4 - e.clientY;
      pos3 = e.clientX;
      pos4 = e.clientY;
      // set the element's new position:
      note.style.top = (note.offsetTop - pos2) + "px";
      note.style.left = (note.offsetLeft - pos1) + "px";
    }
    
    function closeDragElement() {
        /* stop moving when mouse button is released:*/
        notesList[getIndexOfCurrentBoard()][index].xPosition = note.offsetLeft - pos1;
        notesList[getIndexOfCurrentBoard()][index].yPosition = note.offsetTop - pos2;
        document.onmouseup = null;
        document.onmousemove = null;
        save();
    }
}